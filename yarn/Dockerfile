FROM centos:7
LABEL Pedro Maia Martins de Sousa <brpedromaia@gmail.com> and  Rodolfo Silva <Homaru> 

# Environment variables
ENV	HOME /root
ENV	LANG en_US.UTF-8
ENV	LC_ALL en_US.UTF-8

# Default Installation
RUN yum install -y curl tar sudo which initscripts openssh-server openssh-clients rsync 

# Dev tools
RUN yum install -y net-tools telnet mc

# User & SSH Setup
USER root

RUN mkdir -p /root/.ssh
RUN rm -f /etc/ssh/ssh_host_ecdsa_key /etc/ssh/ssh_host_rsa_key && \
    ssh-keygen -q -N "" -t dsa -f /etc/ssh/ssh_host_ecdsa_key && \
    ssh-keygen -q -N "" -t rsa -f /etc/ssh/ssh_host_rsa_key && \
    ssh-keygen -q -N "" -t ed25519 -f /etc/ssh/ssh_host_ed25519_key && \ 
    sed -i "s/#UsePrivilegeSeparation.*/UsePrivilegeSeparation no/g" /etc/ssh/sshd_config

ADD id_rsa /root/.ssh/id_rsa
ADD id_rsa.pub /root/.ssh/id_rsa.pub
ADD id_rsa.pub /root/.ssh/authorized_keys
ADD ssh_config /root/.ssh/config

RUN chmod 600 /root/.ssh/config && chown root:root /root/.ssh/config

# Java Installation
ADD jdk-8u181-linux-x64.rpm ./

RUN rpm -i jdk-8u181-linux-x64.rpm
RUN yum clean all
RUN rm -f jdk-8u181-linux-x64.rpm

ENV JAVA_HOME /usr/java/default
ENV PATH $PATH:$JAVA_HOME/bin

# Hadoop Installation
RUN mkdir -p /usr/local

ADD hadoop.tar.gz /usr/local

ENV HADOOP_HOME /usr/local/hadoop
ENV HADOOP_PREFIX /usr/local/hadoop
ENV HADOOP_COMMON_HOME /usr/local/hadoop
ENV HADOOP_HDFS_HOME /usr/local/hadoop
ENV HADOOP_MAPRED_HOME /usr/local/hadoop
ENV HADOOP_YARN_HOME /usr/local/hadoop
ENV HADOOP_CONF_DIR /usr/local/hadoop/etc/hadoop
ENV YARN_CONF_DIR $HADOOP_PREFIX/etc/hadoop
ENV PATH $PATH:/usr/local/hadoop/bin

RUN	rm -rf  $HADOOP_PREFIX/etc/hadoop/*.cmd && chmod +x $HADOOP_PREFIX/etc/hadoop/*.sh

# Create Volume
VOLUME /hdfs/volume1

# Expose Ports
EXPOSE 22
EXPOSE 8030 8031 8032 8033 8088
EXPOSE 10020
EXPOSE 19888

# Entrypoint 
COPY yarn-entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
