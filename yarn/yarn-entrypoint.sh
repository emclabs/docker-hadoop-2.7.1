#!/bin/bash

# Environment variables
: ${HADOOP_PREFIX:=/usr/local/hadoop}

# Init hadoop-env
$HADOOP_PREFIX/etc/hadoop/hadoop-env.sh
/bin/rm -rf /tmp/*.pid

# Get hostname
echo "Current Hostname: " `hostname`

# Get the namenode host address
if [ -z $HADOOP_HOST_NAMENODE ]; then 
 HADOOP_HOST_NAMENODE=namenode;
 echo "No namenode passed, setting short hostname to namenode. Pass in a value for HADOOP_HOST_NAMENODE to set namenode host.";
fi

echo "Starting Resourcemanager"
$HADOOP_PREFIX/sbin/yarn-daemon.sh start resourcemanager

echo "Starting Job History Server"
$HADOOP_PREFIX/sbin/mr-jobhistory-daemon.sh start historyserver

# Start SSHD 
echo "Starting sshd"
exec /usr/sbin/sshd -D