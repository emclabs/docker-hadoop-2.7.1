Comandos prontos para teste:

docker network create dockerlan

docker build -t namenode:latest /Users/Pedro/Projects/docker-hadoop-2.7.1/namenode/
docker build -t yarn:latest /Users/Pedro/Projects/docker-hadoop-2.7.1/yarn/
docker build -t datanode:latest /Users/Pedro/Projects/docker-hadoop-2.7.1/datanode/
docker build -t mysql:latest /Users/Pedro/Projects/docker-hadoop-2.7.1/mysql/
docker build -t hive:latest /Users/Pedro/Projects/docker-hadoop-2.7.1/hive/

docker run -itd --net=dockerlan --hostname=namenode --name=namenode namenode
docker run -itd --net=dockerlan --hostname=yarn --name=yarn yarn
docker run -itd --net=dockerlan --hostname=datanode --name=datanode datanode
docker run -itd --net=dockerlan --hostname=mysql --name=mysql mysql
docker run -it --net=dockerlan --hostname=hive --name=hive hive

no docker do hive:
{
hive -e "create database ext;"
hive -e "create database fact;"
hive -f /carga/hql/dw_f_icd_presented_history.hql
hdfs dfs -mkdir -p /ingestion/icd/presented_history/hive_staging
hdfs dfs -put /carga/landing/ICD_PRESENTED_HISTORY_20180622.txt.gz /ingestion/icd/presented_history/hive_staging
hive -e "SELECT * FROM ext.dw_f_icd_presented_history"
}


#############.  OUTROS TESTES. [PEDRO] -- NAO APAGAR ######################################
beeline -u jdbc:hive2:// -n hive -p hive -e


connection_hive.properties   [----] 18 L:[  1+ 7   8/  9] *(129 / 130b) 0010 0x00A                                                                                                                    [*][X]
# ip hive for connection
hive_host=""

# username for connection
hive_username=hive

# password for connection
hive_password=hive


hive_load_data.sh   [----] 18 L:[  1+14  15/ 75] *(240 /1857b) 0097 0x061                                                                                                                             [*][X]
#!/bin/bash

SUCESSO=0
ERRO=1

source ${LIBSH}/connection_hive.properties

#Funcao que realiza o load dos dados no HIVE
#
# $1 sql script SQL com o insert/select
#.
# retorno 0 se OK, 1 caso contrario
function loadData {

    insert_query="add jar /usr/hdp/2.6.0.3-8/hive/lib/hive-contrib.jar;-->/usr/local/hive/lib/hive-contrib.jar;
set mapreduce.map.speculative=false;
set mapreduce.reduce.speculative=false;
set hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.max.dynamic.partitions.pernode=1000;
set hive.exec.max.created.files=3000;
set hive.merge.mapfiles=true;
set hive.merge.mapredfiles=true;
set hive.merge.size.per.task=256000000;
set hive.merge.smallfiles.avgsize=60000000;