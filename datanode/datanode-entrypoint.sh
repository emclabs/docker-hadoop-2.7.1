#!/bin/bash

# Environment variables
: ${HADOOP_PREFIX:=/usr/local/hadoop}

# Init hadoop-env
$HADOOP_PREFIX/etc/hadoop/hadoop-env.sh
/bin/rm -rf /tmp/*.pid

# Get the namenode hostname & configure 
if [ -z $HADOOP_HOST_NAMENODE ]; then 
   HADOOP_HOST_NAMENODE=namenode;
 echo "No namenode passed, setting short hostname to namenode. Set env variable for HADOOP_HOST_NAMENODE to change namenode hostname.";
fi

# Start the datanode daemon
$HADOOP_PREFIX/sbin/hadoop-daemon.sh start datanode

# Start nodemanager
$HADOOP_PREFIX/sbin/yarn-daemon.sh start nodemanager

# Start SSHD 
echo "Starting sshd"
exec /usr/sbin/sshd -D
